
A simple game I use as a sandbox to try [Processing](http://processing.org/) ... so don't expect brilliant game play or anything ;)

# Magic Lab

You're a sorcerer's apprentice and she ordered you to clean up the laboratory.

You are not as dumb as you might look sometimes and created a spell which presents each object 
in the laboratory to you one after another, so you can easily choose where it might belong to:

**the good or the evil**, which should be kept clearly separated at all times.

Let's see how you fare, working with your own spell... 

## How to play

* Touch _good_ objects with the left button of your magic wand.
* Touch _evil_ objects with the right button of your magic wand.

Some combinations of objects/ingredients will give extra points if recognized correctly. 


## Laboratory objects  

### Good

![Sun Chest](docs/good_sun-chest.png "Sun Chest") Sun Chest

![Good Jar](docs/good_jar.png "Good Jar") Poison Jar


### Evil

![Skull](docs/evil_skull.png "Skull") Skull

![Evil Jar](docs/evil_jar.png "Evil Jar") Jar


## Screenshot

![Screenshot](docs/sc_start.png "Screenshot")

![Screenshot](docs/sc_game.png "Screenshot")

![Screenshot](docs/sc_game_over.png "Screenshot")

* Upper right corner shows your "lives"
* Lower left corner displays your achieved points, current level and last achieved combo.
