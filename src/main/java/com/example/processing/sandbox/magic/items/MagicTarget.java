package com.example.processing.sandbox.magic.items;

import processing.core.PApplet;
import processing.core.PShape;

import com.example.processing.sandbox.Main;
import com.example.processing.sandbox.magic.HitType;

public abstract class MagicTarget {

    protected static final int LEVEL_SPEED_MULTIPLIER = 15;
    protected static final int TARGET_MIN_SIZE = 20;
    protected static final int TARGET_MAX_SIZE = 70;
    protected static final int TARGET_DEADLINE = 60; // 1 second

    private PApplet parent;
    private PShape shape;

    private int width = 0;
    private int height = 0;
    private int xPos = 0;
    private int yPos = 0;
    private int pointValue = 0;
    private int lifeSpan = 5 * 60; // 5 seconds

    /**
     * Creates a random new target. {@link GoodMagicTarget}s are created slightly more often than
     * {@link EvilMagicTarget}s.
     * 
     * @param pApplet
     * @param speedIncrement
     * @return Newly created {@link MagicTarget}
     */
    public static MagicTarget createMagicTarget(final PApplet pApplet, final int speedIncrement) {

        float type = pApplet.random(-5, 7);

        if (type < 0) {
            return new EvilMagicTarget(pApplet, speedIncrement);
        }

        return new GoodMagicTarget(pApplet, speedIncrement);
    }

    protected MagicTarget(final PApplet pApplet, final int level) {
        this.parent = pApplet;

        this.width = (int) parent.random(TARGET_MIN_SIZE, TARGET_MAX_SIZE);
        this.height = (int) parent.random(TARGET_MIN_SIZE, TARGET_MAX_SIZE);
        this.xPos = (int) parent.random(0, Main.IMAGE_WIDTH - this.width);
        this.yPos = (int) parent.random(0, Main.IMAGE_HEIGHT - this.height);

        // TODO: give more points for higher levels?
        this.pointValue = this.width / 10;

        this.lifeSpan -= (level * LEVEL_SPEED_MULTIPLIER);
    }

    /**
     * Target title. used to identify target in combo queue.
     * 
     * @return Title of this target
     */
    public abstract String getTitle();

    /**
     * Manages display of this target and "fade out" animation.
     * 
     * @return True if this target is still valid, false if expire time for this target is passed
     */
    public boolean display() {

        lifeSpan--;
        if (lifeSpan <= 0) {
            return false;
        }

        getParent().pushMatrix();
        getParent().translate(getxPos(), getyPos());

        // "fade out" animation during last second
        if (lifeSpan < TARGET_DEADLINE) {
            float factor = 1f / lifeSpan;
            getParent().translate(-getWidth() / 2f * factor, -getHeight() / 2f * factor);
            getParent().scale(1 + factor);
        }
        getParent().shape(shape, 0, 0, getWidth(), getHeight());
        getParent().popMatrix();

        return true;
    }

    /**
     * Clears this target and generates new values.
     */
    public void clear() {
        this.width = (int) parent.random(TARGET_MIN_SIZE, TARGET_MAX_SIZE);
        this.height = (int) parent.random(TARGET_MIN_SIZE, TARGET_MAX_SIZE);
        this.xPos = (int) parent.random(0, Main.IMAGE_WIDTH - TARGET_MIN_SIZE);
        this.yPos = (int) parent.random(0, Main.IMAGE_HEIGHT - TARGET_MIN_SIZE);
    }

    /**
     * Hit test with mouse cursor.
     * 
     * @param mouseButton
     *            Which mouse button was pressed
     * @param mouseX
     *            Position of mouse cursor on X axis
     * @param mouseY
     *            Position of mouse cursor on Y axis
     * @return {@link HitType}
     */
    public abstract HitType hit(final int mouseButton, final float mouseX, final float mouseY);

    /**
     * Hit test with mouse cursor.
     * 
     * @param mouseX
     *            Position of mouse cursor on X axis
     * @param mouseY
     *            Position of mouse cursor on Y axis
     * @return {@link HitType}
     */
    protected HitType hit(final float mouseX, final float mouseY) {
        if (mouseX > this.xPos && mouseX < this.xPos + this.width) {
            if (mouseY > this.yPos && mouseY < this.yPos + this.height) {
                return HitType.POSITIVE;
            }
        }
        return HitType.NONE;
    }

    /**
     * Reduces value of this target, so you get less points when hit.
     */
    public void reducePointValue() {
        if (this.pointValue > 0) {
            this.pointValue--;
        }
    }

    /**
     * Gets value of parent.
     * 
     * @return the parent
     */
    public PApplet getParent() {
        return parent;
    }

    /**
     * Sets the value of parent.
     * 
     * @param parent
     *            the parent to set
     */
    public void setParent(final PApplet parent) {
        this.parent = parent;
    }

    /**
     * Gets value of shape.
     * 
     * @return the shape
     */
    public PShape getShape() {
        return shape;
    }

    /**
     * Sets the value of shape.
     * 
     * @param shape
     *            the shape to set
     */
    public void setShape(final PShape shape) {
        this.shape = shape;
    }

    /**
     * Gets value of width.
     * 
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the value of width.
     * 
     * @param width
     *            the width to set
     */
    public void setWidth(final int width) {
        this.width = width;
    }

    /**
     * Gets value of height.
     * 
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the value of height.
     * 
     * @param height
     *            the height to set
     */
    public void setHeight(final int height) {
        this.height = height;
    }

    /**
     * Gets value of xPos.
     * 
     * @return the xPos
     */
    public int getxPos() {
        return xPos;
    }

    /**
     * Sets the value of xPos.
     * 
     * @param xPos
     *            the xPos to set
     */
    public void setxPos(final int xPos) {
        this.xPos = xPos;
    }

    /**
     * Gets value of yPos.
     * 
     * @return the yPos
     */
    public int getyPos() {
        return yPos;
    }

    /**
     * Sets the value of yPos.
     * 
     * @param yPos
     *            the yPos to set
     */
    public void setyPos(final int yPos) {
        this.yPos = yPos;
    }

    /**
     * Gets value of value.
     * 
     * @return the value
     */
    public int getValue() {
        return pointValue;
    }

    /**
     * Sets the value of value.
     * 
     * @param value
     *            the value to set
     */
    public void setValue(final int value) {
        this.pointValue = value;
    }

    /**
     * Gets value of lifeSpan.
     * 
     * @return the lifeSpan
     */
    public int getLifeSpan() {
        return lifeSpan;
    }

    /**
     * Sets the value of lifeSpan.
     * 
     * @param lifeSpan
     *            the lifeSpan to set
     */
    public void setLifeSpan(final int lifeSpan) {
        this.lifeSpan = lifeSpan;
    }
}
