package com.example.processing.sandbox.magic;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public enum Combo {

    TRIPLE_GOOD("Triple Good", 5, new LinkedList<>(Arrays.asList("good", "good", "good"))), //
    TRIPLE_EVIL("Triple Evil", 5, new LinkedList<>(Arrays.asList("evil", "evil", "evil"))), //
    SWAP_GOOD("Alternate", 5, new LinkedList<>(Arrays.asList("good", "evil", "good"))), //
    SWAP_EVIL("Alternate", 5, new LinkedList<>(Arrays.asList("evil", "good", "evil")));

    private String name;
    private int value;
    private List<String> content;

    private Combo(final String name, final int value, final List<String> content) {
        this.name = name;
        this.value = value;
        this.content = content;
    }

    public String getName() {
        return this.name;
    }

    public int getValue() {
        return this.value;
    }

    public List<String> getContent() {
        return this.content;
    }
}
