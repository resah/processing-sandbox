package com.example.processing.sandbox.magic;

import java.util.LinkedList;

import processing.core.PApplet;

import com.example.processing.sandbox.Main;
import com.example.processing.sandbox.magic.items.MagicTarget;

public class Score {

    private static final int LEVEL_INCREASE = 10000; // each ten seconds

    private int hitCounter = 0;
    private int level = 1;
    private int lives = 5;
    private Combo lastCombo;
    private final LinkedList<String> comboQueue = new LinkedList<>();

    /**
     * Increments {@link #hitCounter} by one.
     */
    public void incrementOne() {
        increment(1);
    }

    /**
     * Increments {@link #hitCounter} by a value.
     * 
     * @param by
     *            Value to increase {@link #hitCounter}
     */
    public void increment(final int by) {
        hitCounter += by;
    }

    /**
     * Decreases {@link #hitCounter} by one.
     */
    public void decreaseOne() {
        decrease(1);
    }

    /**
     * Decreases {@link #hitCounter} by a value.
     * 
     * @param by
     *            Value to decrease {@link #hitCounter}
     */
    public void decrease(final int by) {

        clearCombos();
        hitCounter -= by;
        lives--;
    }

    /**
     * Gets value of lives.
     * 
     * @return the lives
     */
    public int getLives() {
        return lives;
    }

    /**
     * Gets value of level.
     * 
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Increases level after specific game time.
     * 
     * @param millis
     *            In game time in millis
     */
    public void increaseLevel(final int millis) {
        level = millis / LEVEL_INCREASE;
    }

    /**
     * Clears all combos from queue.
     */
    public void clearCombos() {
        comboQueue.clear();
    }

    /**
     * Rest last combo to NULL.
     */
    public void resetLastCombo() {
        this.lastCombo = null;
    }

    /**
     * Checks whether last hits were a combo. Handles hit counter increment and clears combo queue.
     * 
     * @param target
     *            {@link MagicTarget}
     */
    public void checkCombo(final MagicTarget target) {

        resetLastCombo();
        comboQueue.add(target.getTitle());
        while (comboQueue.size() > 3) {
            comboQueue.removeFirst();
        }

        // check for combo
        for (Combo combo : Combo.values()) {
            if (comboQueue.equals(combo.getContent())) {

                // clear queue
                clearCombos();

                // increment counter
                this.increment(combo.getValue());
                this.lastCombo = combo;
            }
        }
    }

    /**
     * Generates output to display current score information.
     * 
     * @param pApplet
     */
    public String printScore() {

        return "Points: " + hitCounter + "\n" //
                + "Level: " + (level + 1) + "\n" //
                + "Combo: " + ((lastCombo != null) ? lastCombo.getName() : "");
    }

    /**
     * Generates output to display number of lives.
     * 
     * @param pApplet
     */
    public void printLives(final PApplet pApplet) {

        pApplet.fill(120);
        for (int life = 0; life < getLives(); life++) {
            pApplet.ellipse(Main.IMAGE_WIDTH - life * 20 - 20, 30, 20, 20);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Score [counter=" + hitCounter + ", level=" + level + ", lives=" + lives + "]";
    }

}
