package com.example.processing.sandbox.magic.screen;

import processing.core.PApplet;

import com.example.processing.sandbox.magic.Score;

public abstract class AbstractScreen {

    protected PApplet parent;
    protected Score score;
    private String nextScreen;

    public AbstractScreen(final PApplet parentApplet) {
        this.parent = parentApplet;
    }

    /**
     * Initializes everything which is needed by the frame. (Equivalent to PApplet.setup())
     */
    public void setup() {
    }

    /**
     * Called for every frame. (Equivalent to PApplet.draw())
     */
    public void draw() {
    }

    /**
     * Called when a key was pressed. (Equivalent to PApplet.keyPressed())
     */
    public void keyPressed() {
    }

    /**
     * Called when mouse button was clicked. (Equivalent to PApplet.mouseClicked())
     */
    public void mouseClicked() {
    }

    /**
     * Handles screen switching.
     * 
     * @return Name of new screen or null
     */
    public String switchScreen() {
        String selectedScreen = nextScreen;
        nextScreen = null;
        return selectedScreen;
    }

    /**
     * Sets the value of nextScreen.
     * 
     * @param nextScreen
     *            the nextScreen to set
     */
    protected void setNextScreen(final String nextScreen) {
        this.nextScreen = nextScreen;
    }

    /**
     * Gets value of score.
     * 
     * @return the score
     */
    public Score getScore() {
        if (score == null) {
            score = new Score();
        }
        return score;
    }

    /**
     * Sets the value of score.
     * 
     * @param score
     *            the score to set
     */
    public void setScore(final Score score) {
        this.score = score;
    };
}