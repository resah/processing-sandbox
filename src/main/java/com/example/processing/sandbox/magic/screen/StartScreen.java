package com.example.processing.sandbox.magic.screen;

import processing.core.PApplet;
import processing.core.PImage;

import com.example.processing.sandbox.Main;
import com.example.processing.sandbox.utils.ShapeLoader;

/**
 * Display game start and synopsis
 */
public class StartScreen extends AbstractScreen {

    private PImage background;

    private final String text =
            "You're a sorcerer's apprentice and she asked you to clean up the laboratory.\n"
                    + "You are not as dumb as you think you look and created a spell which presents\n"
                    + "each object in the laboratory to you one after another, so you can easily choose\n"
                    + "where it might belong to:\n"
                    + "The good or the evil - which should be kept clearly separated at all times.\n\n"
                    + "Let's see how you fare, working with your own spell...";

    public StartScreen(final PApplet parentApplet) {
        super(parentApplet);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#setup()
     */
    @Override
    public void setup() {
        background = parent.loadImage("magic/laboratory.png");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#draw()
     */
    @Override
    public void draw() {

        // draw background
        parent.image(background, -50, 0);
        parent.fill(0, 140);
        parent.noStroke();
        parent.rect(0, 0, Main.IMAGE_WIDTH, Main.IMAGE_HEIGHT);

        parent.shape(ShapeLoader.getShapeFor(parent, "magic/magic_lab.svg"), 20, 20, 400, 200);

        // display text
        parent.fill(255);
        parent.text(text, 20, Main.IMAGE_HEIGHT - 190);
        parent.text("Press <SPACE> to start...", 20, Main.IMAGE_HEIGHT - 20);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#keyPressed()
     */
    @Override
    public void keyPressed() {

        if (parent.key == ' ') {
            setNextScreen("game");
        }
    }
}
