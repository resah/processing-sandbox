package com.example.processing.sandbox.magic.screen;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PShape;

import com.example.processing.sandbox.Main;
import com.example.processing.sandbox.magic.HitType;
import com.example.processing.sandbox.magic.items.MagicTarget;
import com.example.processing.sandbox.utils.ShapeLoader;

/**
 * Handles actual game play and updating score.
 */
public class GameScreen extends AbstractScreen {

    private boolean looping = true;
    private int startTime = -1;

    private PImage background;
    private PShape cursor;
    private MagicTarget target;

    public GameScreen(final PApplet parentApplet) {
        super(parentApplet);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#setup()
     */
    @Override
    public void setup() {

        background = parent.loadImage("magic/table.png");
        cursor = ShapeLoader.getShapeFor(parent, "magic/wand.svg");
        target = MagicTarget.createMagicTarget(parent, getScore().getLevel());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#draw()
     */
    @Override
    public void draw() {

        if (startTime < 0) {
            startTime = parent.millis();
        }

        parent.noCursor();

        // draw background
        parent.image(background, 0, 0, Main.IMAGE_WIDTH, Main.IMAGE_HEIGHT);
        parent.fill(0, 200);
        parent.noStroke();
        parent.rect(0, 0, Main.IMAGE_WIDTH, Main.IMAGE_HEIGHT);

        // draw target
        if (!target.display()) {
            // generate new target, because old one is expired
            target = MagicTarget.createMagicTarget(parent, getScore().getLevel());
            getScore().clearCombos();
            return;
        }

        // draw cursor
        parent.shape(cursor, parent.mouseX - 7, parent.mouseY - 5, 30, 42);

        // check health
        if (getScore().getLives() == 0 || getScore().getLevel() >= 15) {
            // game over
            target = MagicTarget.createMagicTarget(parent, getScore().getLevel());
            setNextScreen("gameover");
            parent.cursor();
            startTime = -1;
        }

        // display game state
        parent.fill(255);
        parent.text(getScore().printScore(), 20, Main.IMAGE_HEIGHT - 50);
        getScore().printLives(parent);

        // check whether to increase level
        getScore().increaseLevel(getGameTime());

        // debug
        // parent.fill(100);
        // parent.text("Timer: " + getGameTime(), 20, 20);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#mouseClicked()
     */
    @Override
    public void mouseClicked() {

        HitType hitType = target.hit(parent.mouseButton, parent.mouseX, parent.mouseY);
        if (HitType.POSITIVE.equals(hitType)) {

            // success
            getScore().checkCombo(target);
            getScore().increment(target.getValue());
            target = MagicTarget.createMagicTarget(parent, getScore().getLevel());
        }
        if (HitType.NEGATIVE.equals(hitType)) {
            // fail
            getScore().resetLastCombo();
            getScore().decrease(target.getValue());
            target.reducePointValue();
        }

        // penalty for clicks without hit?
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#keyPressed()
     */
    @Override
    public void keyPressed() {
        if (parent.key == 'p') {
            if (looping) {
                // pause game
                parent.text("PAUSE", Main.IMAGE_WIDTH / 2 - 20, Main.IMAGE_HEIGHT / 2);
                parent.redraw();
                parent.noLoop();
            } else {
                parent.loop();
            }
            looping = !looping;
        }
    }

    private int getGameTime() {
        return parent.millis() - startTime;
    }
}
