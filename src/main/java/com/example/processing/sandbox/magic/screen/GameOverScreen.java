package com.example.processing.sandbox.magic.screen;

import processing.core.PApplet;
import processing.core.PImage;

import com.example.processing.sandbox.Main;
import com.example.processing.sandbox.utils.ShapeLoader;

/**
 * Display of result score and replay option
 */
public class GameOverScreen extends AbstractScreen {

    private PImage background;

    public GameOverScreen(final PApplet parentApplet) {
        super(parentApplet);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#setup()
     */
    @Override
    public void setup() {
        background = parent.loadImage("magic/laboratory.png");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#draw()
     */
    @Override
    public void draw() {

        // draw background
        parent.image(background, -50, 0);
        parent.fill(0, 140);
        parent.noStroke();
        parent.rect(0, 0, Main.IMAGE_WIDTH, Main.IMAGE_HEIGHT);

        // title
        parent.shape(ShapeLoader.getShapeFor(parent, "magic/game_over.svg"), Main.IMAGE_WIDTH / 2 - 175, 30,
                350, 200);

        // score
        parent.fill(255);
        parent.text(getScore().printScore(), 20, Main.IMAGE_HEIGHT - 50);

        // parent.text("Press <n> to restart...", 20, Main.IMAGE_HEIGHT - 20);
        parent.shape(ShapeLoader.getShapeFor(parent, "magic/play_again.svg"), Main.IMAGE_WIDTH - 140,
                Main.IMAGE_HEIGHT - 100, 120, 80);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#keyPressed()
     */
    @Override
    public void keyPressed() {
        if (parent.key == 'n') {
            // invalidate score
            setScore(null);
            setNextScreen("game");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.screen.AbstractScreen#mouseClicked()
     */
    @Override
    public void mouseClicked() {
        if (parent.mouseX >= (Main.IMAGE_WIDTH - 140) && parent.mouseX <= (Main.IMAGE_WIDTH - 20)) {
            if (parent.mouseY >= (Main.IMAGE_HEIGHT - 100) && parent.mouseY <= (Main.IMAGE_HEIGHT - 20)) {
                // invalidate score
                setScore(null);
                setNextScreen("game");
            }
        }
    }
}
