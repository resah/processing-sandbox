package com.example.processing.sandbox.magic;

public enum HitType {

    NONE, // no hit
    POSITIVE, // good hit -> you get points
    NEGATIVE // wrong hit -> you loose points
}
