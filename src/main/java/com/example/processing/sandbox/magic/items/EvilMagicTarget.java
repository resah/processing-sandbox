package com.example.processing.sandbox.magic.items;

import processing.core.PApplet;

import com.example.processing.sandbox.magic.HitType;
import com.example.processing.sandbox.utils.ShapeLoader;

public class EvilMagicTarget extends MagicTarget {

    public EvilMagicTarget(final PApplet pApplet, final int speedIncrement) {

        super(pApplet, speedIncrement);

        float shape = pApplet.random(-5, 10);
        if (shape < 0) {
            this.setShape(ShapeLoader.getShapeFor(pApplet, "magic/evil/skull_and_crossbones.svg"));
        } else {
            this.setShape(ShapeLoader.getShapeFor(pApplet, "magic/evil/jar.svg"));
        }
        // shape.disableStyle();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.items.MagicTarget#getTitle()
     */
    @Override
    public String getTitle() {
        return "evil";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.items.MagicTarget#clear()
     */
    @Override
    public void clear() {
        super.clear();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.example.processing.sandbox.magic.items.MagicTarget#hit(int, float, float)
     */
    @Override
    public HitType hit(final int mouseButton, final float mouseX, final float mouseY) {

        HitType hitType = super.hit(mouseX, mouseY);
        if (hitType.equals(HitType.NONE)) {
            return HitType.NONE;
        }

        if (mouseButton != PApplet.RIGHT) {
            return HitType.NEGATIVE;
        }

        return HitType.POSITIVE;
    }

}
