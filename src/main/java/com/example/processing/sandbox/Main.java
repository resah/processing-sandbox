package com.example.processing.sandbox;

import java.util.HashMap;
import java.util.Map;

import processing.core.PApplet;
import processing.core.PFont;

import com.example.processing.sandbox.magic.Score;
import com.example.processing.sandbox.magic.screen.AbstractScreen;
import com.example.processing.sandbox.magic.screen.GameOverScreen;
import com.example.processing.sandbox.magic.screen.GameScreen;
import com.example.processing.sandbox.magic.screen.StartScreen;
import com.example.processing.sandbox.utils.ShapeLoader;

/**
 * TODO: Features ...
 * 
 * - screens: help/how to play
 * - audio
 * - disrupter: dust or glimmer as dirt which won't react to clicking
 * - combo can contain other combos or multiple combos can be a combo itself
 * 
 */
public class Main extends PApplet {

    private static final long serialVersionUID = 86099995301365415L;

    public static final int IMAGE_WIDTH = 600;
    public static final int IMAGE_HEIGHT = 450;

    // creating a list of our apps
    AbstractScreen selectedScreen;
    private final Map<String, AbstractScreen> screens = new HashMap<>();

    /*
     * (non-Javadoc)
     * 
     * @see processing.core.PApplet#setup()
     */
    @Override
    public void setup() {

        size(IMAGE_WIDTH, IMAGE_HEIGHT, P2D);
        smooth();
        noCursor();
        background(0, 0, 0);
        PFont font = createFont("Arial", 14, true);
        textFont(font, 14);

        // preload shapes
        ShapeLoader.getShapeFor(this, "magic/magic_lab.svg");
        ShapeLoader.getShapeFor(this, "magic/game_over.svg");
        ShapeLoader.getShapeFor(this, "magic/play_again.svg");
        ShapeLoader.getShapeFor(this, "magic/help.svg");
        ShapeLoader.getShapeFor(this, "magic/wand.svg");
        ShapeLoader.getShapeFor(this, "magic/good/jar.svg");
        ShapeLoader.getShapeFor(this, "magic/good/chest.svg");
        ShapeLoader.getShapeFor(this, "magic/evil/jar.svg");
        ShapeLoader.getShapeFor(this, "magic/evil/skull_and_crossbones.svg");

        // screens
        screens.put("start", new StartScreen(this)); // start screen
        screens.put("game", new GameScreen(this)); // game
        screens.put("gameover", new GameOverScreen(this)); // game over + points + replay

        // calling the initialization function on each
        // Applet in the list.
        for (AbstractScreen screen : screens.values()) {
            screen.setup();
        }

        // start screen
        selectedScreen = screens.get("start");
    }

    /*
     * (non-Javadoc)
     * 
     * @see processing.core.PApplet#draw()
     */
    @Override
    public void draw() {

        // we call the display method of the selected app.
        selectedScreen.draw();

        // check whether we will have to switch screen
        String newScreen = selectedScreen.switchScreen();
        if (newScreen != null && screens.containsKey(newScreen)) {
            Score currentScore = selectedScreen.getScore();
            selectedScreen = screens.get(newScreen);
            selectedScreen.setScore(currentScore);
        }

        // helper for screenshots
        // if (frameCount % 60 == 0) {
        // saveFrame("line-######.png");
        // }
    }

    /*
     * (non-Javadoc)
     * 
     * @see processing.core.PApplet#keyPressed()
     */
    @Override
    public void keyPressed() {
        selectedScreen.keyPressed();
    }

    /*
     * (non-Javadoc)
     * 
     * @see processing.core.PApplet#mouseClicked()
     */
    @Override
    public void mouseClicked() {
        selectedScreen.mouseClicked();
    }

    public static void main(final String args[]) {
        PApplet.main(new String[] { "--present", "com.example.processing.sandbox.Main" });
    }
}
