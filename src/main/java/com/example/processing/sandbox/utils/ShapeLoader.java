package com.example.processing.sandbox.utils;

import java.util.HashMap;
import java.util.Map;

import processing.core.PApplet;
import processing.core.PShape;

public class ShapeLoader {

    private static Map<String, PShape> SHAPES = new HashMap<>();

    private ShapeLoader() {
    }

    public static synchronized PShape getShapeFor(final PApplet pApplet, final String fileName) {

        if (SHAPES.containsKey(fileName)) {
            return SHAPES.get(fileName);
        }

        PShape shape = pApplet.loadShape(fileName);
        SHAPES.put(fileName, shape);
        return shape;
    }
}
